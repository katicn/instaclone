# Running

    $ cd instaclone
    $ pipenv install
    $ pipenv shell
    $ python manage.py migrate
    $ python manage.py runserver
    
# Tests

    $ python manage.py test
    
# API

## Authentication 

Users are authenticated using a token inside the `Authorization` HTTP header.

Example:
```
Authorization: Token 6c6be3aa4947986b83a1a294d421eb882f457ea8
```

## Methods

### GET /followed_posts 

Returns a paginated list of posts made by followed users.

Example response:
```json
{
  "count": 75,
  "next": "http://instaclone/api/posts?page=2",
  "previous": null,
  "results": [
    {
      "id": 75,
      "owner": {
        "id": 1,
        "username": "alice"
      },
      "created_at": "2010-10-11T10:10:10.000000Z",
      "description": "my cat"
    },
    {
      "id": 74,
      "owner": {
        "id": 2,
        "username": "bob"
      },
      "created_at": "2010-10-10T10:10:10.000000Z",
      "description": "my dog"
    }
  ]
}
```

### GET /posts

Returns a paginated list of all posts.

### POST /posts

Creates a new post. Request body must be a multipart form.

Field | Type 
--- | ---
image | file 
description | text

### GET /posts/:post_id/image

Retrieves the image associated with the post.

### POST /users

Registers a new user.

Example request:
```json
{
  "username": "alice",
  "password": "secret"
}
```

### PUT /users/:user_id/follow

Start following a user.

### DELETE /users/:user_id/follow

Stop following a user.

### POST /sessions

Creates a new session.

Example request:

```json
{
  "username": "alice",
  "password": "secret"
}
```

Example response:

```json
{
  "token": "6c6be3aa4947986b83a1a294d421eb882f457ea8"
}
```