from django.urls import path
from rest_framework.authtoken.views import obtain_auth_token

from . import views

urlpatterns = [
    path('followed_posts', views.FollowedPostsView.as_view(), name='followed_posts'),
    path('posts', views.PostsView.as_view(), name='posts'),
    path('posts/<int:pk>/image', views.ImagesView.as_view(), name='posts-image'),
    path('users', views.UsersView.as_view(), name='users'),
    path('users/<int:pk>/follow', views.FollowView.as_view(), name='users-follow'),
    path('sessions', obtain_auth_token, name='sessions'),
]
