from django.apps import AppConfig


class InstacloneApiConfig(AppConfig):
    name = 'instaclone_api'
