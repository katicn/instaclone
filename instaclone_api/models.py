from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    followees = models.ManyToManyField('self', related_name='followers',
                                       through_fields=('follower', 'followee'),
                                       through='Follow', symmetrical=False)


class Post(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    description = models.CharField(max_length=300)
    image = models.BinaryField()  # for simplicity
    owner = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)

    def __str__(self):
        return self.description


class Follow(models.Model):
    follower = models.ForeignKey(User, related_name='followee_follows', on_delete=models.CASCADE)
    followee = models.ForeignKey(User, related_name='follower_follows', on_delete=models.CASCADE)
