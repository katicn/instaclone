# Generated by Django 2.2.5 on 2019-09-06 07:11
from django.contrib.auth import get_user_model
from django.db import migrations


def create_superuser(apps, schema_editor):
    get_user_model().objects.create_superuser('admin', '', 'admin')


class Migration(migrations.Migration):

    dependencies = [
        ('instaclone_api', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(create_superuser),
    ]