from io import BytesIO

from django.contrib.auth import get_user_model
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from instaclone_api.models import Post

User = get_user_model()

small_image = BytesIO(b'\x42\x4D\x1E\x00\x00\x00\x00\x00'
                      b'\x00\x00\x1A\x00\x00\x00\x0C\x00'
                      b'\x00\x00\x01\x00\x01\x00\x01\x00'
                      b'\x18\x00\x00\x00\xFF\x00')  # smallest possible bmp
small_image.name = 'image.bmp'


class UsersTestCase(APITestCase):
    def test_registration(self):
        data = {'username': 'user', 'password': 'pass'}
        response = self.client.post(reverse('users'), data)
        self.assertEqual(response.status_code, 201)


class SessionsTestCase(APITestCase):
    def setUp(self):
        User.objects.create_user('user', '', 'pass')

    def test_login(self):
        data = {'username': 'user', 'password': 'pass'}
        response = self.client.post(reverse('sessions'), data)
        self.assertEqual(response.status_code, 200)
        self.assertTrue('token' in response.data)


class UnAuthPostsTestCase(APITestCase):
    def test_unauthenticated_user_cant_post(self):
        data = {'description': 'desc', 'image': small_image}
        response = self.client.post(reverse('posts'), data, format='multipart')
        self.assertEqual(response.status_code, 401)

    def test_unauthenticated_user_cant_view_private_timeline(self):
        response = self.client.get(reverse('followed_posts'))
        self.assertEqual(response.status_code, 401)


class AuthPostsTestCase(APITestCase):
    def setUp(self):
        self.user1 = User.objects.create_user('user1', '', 'pass')
        self.user2 = User.objects.create_user('user2', '', 'pass')
        self.user3 = User.objects.create_user('user3', '', 'pass')
        self.client.force_authenticate(self.user1)
        Post.objects.create(description='Post by user2', owner=self.user2)
        Post.objects.create(description='Post by user3', owner=self.user3)

    def test_public_timeline(self):
        response = self.client.get(reverse('posts'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['count'], 2)

    def test_private_timeline(self):
        response = self.client.get(reverse('followed_posts'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['count'], 0)

        # test follow
        self.client.put(reverse('users-follow', args=[self.user2.id]))
        response = self.client.get(reverse('followed_posts'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['count'], 1)
        self.assertEqual(response.data['results'][0]['description'], 'Post by user2')

        # test unfollow
        self.client.delete(reverse('users-follow', args=[self.user2.id]))
        response = self.client.get(reverse('followed_posts'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['count'], 0)

    def test_create_post(self):
        data = {'description': 'desc', 'image': small_image}
        response = self.client.post(reverse('posts'), data, format='multipart')
        self.assertEqual(response.status_code, 201)

        response = self.client.get(reverse('posts'))
        self.assertEqual(response.data['count'], 3)
