from django.contrib.auth import get_user_model
from django.http import HttpResponse
from rest_framework import generics, status, permissions
from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from instaclone_api.models import Post
from instaclone_api.serializers import PostSerializer, UserSerializer

User = get_user_model()


class FollowedPostsView(generics.ListAPIView):
    serializer_class = PostSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return Post.objects.filter(owner__followers=self.request.user).order_by('-created_at')


class PostsView(generics.ListCreateAPIView):
    queryset = Post.objects.all().order_by('-created_at')
    serializer_class = PostSerializer
    parser_classes = (MultiPartParser,)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def get_permissions(self):
        # forbid unauthenticated users from creating posts
        if self.request.method in permissions.SAFE_METHODS:
            return ()
        else:
            return IsAuthenticated(),


class ImagesView(generics.RetrieveAPIView):
    queryset = Post.objects.all()

    def retrieve(self, request, *args, **kwargs):
        post = self.get_object()
        return HttpResponse(post.image, content_type='image/*')


class UsersView(generics.CreateAPIView):
    serializer_class = UserSerializer


class FollowView(generics.GenericAPIView):
    permission_classes = (IsAuthenticated,)

    def put(self, request, *args, pk=None):
        self.request.user.followees.add(pk)
        return Response(status=status.HTTP_200_OK)

    def delete(self, request, *args, pk=None):
        self.request.user.followees.remove(pk)
        return Response(status=status.HTTP_204_NO_CONTENT)
