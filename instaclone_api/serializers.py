from django.contrib.auth import get_user_model
from rest_framework import serializers

from instaclone_api.models import Post

User = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'password')
        extra_kwargs = {
            'password': {'write_only': True}
        }

    def create(self, validated_data):
        return User.objects.create_user(validated_data['username'], '', validated_data['password'])


class PostSerializer(serializers.ModelSerializer):
    owner = UserSerializer(read_only=True)
    image = serializers.FileField(write_only=True)

    class Meta:
        model = Post
        fields = '__all__'

    def create(self, validated_data):
        validated_data['image'] = validated_data['image'].read()
        return super().create(validated_data)
